**1. Apakah perbedaan antara JSON dan XML?**

JSON dan XML sama-sama digunakan di banyak aplikasi web dan mobile yaitu untuk menyimpan dan mengirimkan data. Namun keduanya memiliki perbedaan sebagai berikut :

| JSON | XML |
| ------ | ------ |
| Didesain self-describing | Didesain self-descriptive |
| Sintaksnya turunan dari Object JavaScript | Sintaksnya dibungkus dalam tag |
| Format merupakan data _interchange_ | Format merupakan _markup language_ |
| _Data-oriented_ | _Document-oriented_ |
| Menggunakan struktur data map | Menggunakan Struktur data tree |
| Memungkinkan penggunaan array | Tidak memungkinkan penggunaan _array_ |
| Hanya memungkinkan tipe data teks dan numerik | Mendukung banyak tipe data seperti teks, numerik, gambar, dan lain-lain|

**2. Apakah perbedaan antara HTML dan XML?**

HTML dan XML keduanya memiliki kegunaan untuk aplikasi web dan pertukaran data. Walaupun memiliki kegunaan yang sama, keduanya memiliki perbedaan. Berikut diantaranya :

| HTML | XML |
| ------ | ------ |
| Memfokuskan format tampilan dari data | Memfokuskan pada struktur dan konteksnya |
| Tersusun atas tag-tag namun tidak menginformasikan isi dari data | Tersusun atas tag-tag yang menginformasikan isi dari data (tag dapat disesuaikan dengan model data yang dibuat) |
| Berfokus pada penyajian data | Berfokus pada transfer data |
| Case Insensitive | Case Sensitive |
| Tidak mendukung namespaces | Mendukung namespaces |


Sumber :
- https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
- https://www.monitorteknologi.com/perbedaan-json-dan-xml/
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://askanydifference.com/id/perbedaan-antara-json-dan-xml/
- https://blogs.masterweb.com/perbedaan-xml-dan-html/
- https://id.wikibooks.org/wiki/Pemrograman_XML/XML_vs_HTML

