import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/widgets/card.dart';
import 'package:lab_6/widgets/card_comment.dart';
import 'package:lab_6/widgets/carousel.dart';
import 'package:lab_6/widgets/comment_textfield.dart';

class HealthyAdviceHome extends StatefulWidget {
  const HealthyAdviceHome({Key? key}) : super(key: key);

  @override
  _HealthyAdviceHomeState createState() => _HealthyAdviceHomeState();
}

class _HealthyAdviceHomeState extends State<HealthyAdviceHome> {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[


        CarouselHealthyAdv(),
        SizedBox(height: 24),
        CardSehat(),
        CardSehat(),
        CardSehat(),
        SizedBox(height: 20,),
        CommentTextField(),
        CardComment(),
        CardComment(),
        CardComment(),
        CardComment(),
        SizedBox(height: 20,),


              // width: ,

        ]
    );
  }
}
