from django.db import models
from datetime import datetime, date


# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

# npm = None
# birth_date = date()


class Friend(models.Model):
    # TODO Implement missing attributes in Friend model
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    birth_date = models.DateField()


    