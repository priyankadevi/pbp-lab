from django.http.response import HttpResponseRedirect
from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    # ngeload
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    response ={}
    form = NoteForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-4")
    response['form']= form
    return render(request, 'lab4_form.html', response)

def note_list(request):
    # ngeload
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)