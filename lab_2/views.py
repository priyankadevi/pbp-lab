from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

# to_name = "Priyanka Devi"
# from_name = 'Ardelia Yudiva'
# title_mail = 'Seek help bro!'
# message_mail = 'https://konselingmakaraui.wixsite.com/seekhelp'


# Create your views here.
def index(request):
    # ngeload
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Note.objects.all()
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")
    